﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 用户-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class UserController : Controller
    {
        /// <summary>
        /// 用户中心主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.user:view")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 用户增加视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.user:add")]
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 用户编辑视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.user:edit")]
        public IActionResult Edit()
        {
            return View();
        }

        /// <summary>
        /// 用户分配角色视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.user:giveroles")]
        public IActionResult Give()
        {
            return View();
        }


        /// <summary>
        /// 用户基本资料
        /// </summary>
        /// <returns></returns>
        public IActionResult UserProfile()
        {
            return View();
        }

    }
}
