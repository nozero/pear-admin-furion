﻿using Furion.DependencyInjection;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pear.Web.Core
{
    /// <summary>
    /// Cookie 操作类
    /// </summary>
    public class CookieUtil : IScoped
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CookieUtil(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 增加Cookie abelp 2020/12/09 17:55
        /// </summary>
        /// <param name="somekey">Cookie名</param>
        /// <param name="somevalue">Cookie值</param>
        public void Set(string somekey, string somevalue)
        {
            _httpContextAccessor.HttpContext.Response.Cookies.Append(somekey, somevalue);
        }

        /// <summary>
        /// 获取Cookie abelp 2020/12/09 17:56
        /// </summary>
        /// <param name="somekey">Cookie名</param>
        /// <returns></returns>
        public string Get(string somekey)
        {
            try
            {
                return _httpContextAccessor.HttpContext.Request.Cookies[somekey];
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        /// <summary>
        /// 删除Cookie abelp 2020/12/09 18:01
        /// </summary>
        /// <param name="somekey">Cookie名</param>
        public void Del(string somekey)
        {
            _httpContextAccessor.HttpContext.Response.Cookies.Delete(somekey);
        }



    }
}
