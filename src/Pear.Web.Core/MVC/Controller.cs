﻿using Furion.DatabaseAccessor;
using Pear.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Pear.Web.Core
{
    /// <summary>
    /// 控制器基类
    /// </summary>
    public abstract class BaseController : Microsoft.AspNetCore.Mvc.Controller
    {
        /// <summary>
        /// 用户仓储
        /// </summary>
        private readonly IRepository<User> _userRepository;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="userRepository"></param>
        public BaseController(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseController()
        { }


        /// <summary>
        /// 获取用户 Id
        /// </summary>
        public int UserId { get => int.Parse(HttpContext.User.FindFirst("UserId").Value); }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        public new User User { get => _userRepository.Find(UserId); }

    }
}
