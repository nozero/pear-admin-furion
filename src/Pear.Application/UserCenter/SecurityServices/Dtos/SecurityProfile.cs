﻿using Pear.Core;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 权限信息
    /// </summary>
    public class SecurityProfile
    {

        public SecurityProfile()
        {
            ParentId = 0;
        }

        /// <summary>
        /// 权限 Id
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 打开方式
        /// </summary>
        public string Target { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public SecurityType? Type { get; set; }
        /// <summary>
        /// 权限标识
        /// </summary>
        public string Authorize { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 序号/排序
        /// </summary>
        public int Sequence { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }


        /// <summary>
        /// 上级Id
        /// </summary>
        public int? ParentId { get; set; } = 0;

        /// <summary>
        /// 复选框
        /// </summary>
        public string checkArr { get; set; } = "0";

    }
}