﻿using Pear.Core;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 部门信息
    /// </summary>
    public class DepartmentProfile
    {

        public DepartmentProfile()
        {
            ParentId = 0;
        }

        /// <summary>
        /// 权限 Id
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 序号/排序
        /// </summary>
        public int Sequence { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }


        /// <summary>
        /// 上级Id
        /// </summary>
        public int? ParentId { get; set; } = 0;

        /// <summary>
        /// 复选框
        /// </summary>
        public string checkArr { get; set; } = "0";

    }
}