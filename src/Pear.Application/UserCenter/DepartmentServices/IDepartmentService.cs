﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 部门服务接口
    /// </summary>
    public interface IDepartmentService
    {

        /// <summary>
        /// 获取所有部门列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedList<DepartmentProfile>> GetListAsync([FromQuery, Required] GetDepartmentListInput input);

        /// <summary>
        /// 获取所有部门列表（不分页）
        /// </summary>
        /// <returns></returns>
        Task<List<DepartmentProfile>> GetAllAsync();




        /// <summary>
        /// 获取部门实体
        /// </summary>
        /// <param name="departmentId">部门编号</param>
        /// <returns></returns>
        Task<DepartmentProfile> ProfileAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的部门 Id")] int departmentId);



        /// <summary>
        /// 修改部门
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        Task ModifyAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的部门 Id"), ApiSeat(ApiSeats.ActionStart)] int departmentId, [Required] EditDepartmentInput input);
 
        /// <summary>
        /// 改变部门状态
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        Task ChangeEnabledAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的部门 Id"), ApiSeat(ApiSeats.ActionStart)] int departmentId, [Required] ChangeEnabledInput input);

        /// <summary>
        /// 新增部门
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<DepartmentProfile> AddAsync([Required] EditDepartmentInput input);



        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
       Task DeleteAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的部门 Id"), ApiSeat(ApiSeats.ActionStart)] int departmentId);
    }
}