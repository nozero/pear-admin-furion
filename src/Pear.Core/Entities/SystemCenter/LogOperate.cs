using Furion.DatabaseAccessor;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pear.Core
{
    /// <summary>
    /// 后台操作日志
    /// </summary>
    public class LogOperate : Entity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public LogOperate()
        {

        }

        /// <summary>
        /// 日志状态
        /// </summary>
        public LogStatus LogStatus { get; set; }

        /// <summary>
        /// 访问URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 带入参数
        /// </summary>
        public string Params { get; set; }


        /// <summary>
        /// IP地址
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// IP所属地
        /// </summary>
        public string IpLocation { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// 操作系统
        /// </summary>
        public string OS { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperateTime { get; set; }

        /// <summary>
        /// 登录用户编号
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// 登录用户
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// 执行时间 ms
        /// </summary>
        public int ExecutionTime { get; set; }
    }
}